/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.Modelo.Persona;
import ufps.util.coleciones_seed.ListaCD;

/**
 *
 * @author Carlos
 */
public class TestCortarListaCD {
public static void main(String[] args) throws Exception {
        ListaCD<Persona> personas=new ListaCD();
        personas.insertarInicio(new Persona(1,"madarme"));
        personas.insertarInicio(new Persona(3,"gederson"));
        personas.insertarInicio(new Persona(4,"diana"));
         personas.insertarInicio(new Persona(5,"jose"));
         personas.insertarInicio(new Persona(8,"alfredo"));
         personas.insertarInicio(new Persona(15,"miguel"));
        System.out.println("Lista 2:");
        System.out.println(personas.cortar(0, 5));
        System.out.println("Lista 1:");
        System.out.println(personas);
}    
}
