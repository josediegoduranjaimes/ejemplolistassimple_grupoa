/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.coleciones_seed;

/**
 *
 * @author madarme
 */
public class ListaCD<T> {

    private NodoD<T> cabecera;
    private int tamano = 0;

    public ListaCD() {
        this.cabecera = new NodoD();
        this.cabecera.setInfo(null);
        this.cabecera.setSiguiente(this.cabecera);
        this.cabecera.setAnterior(cabecera);

    }

    public int getTamano() {
        return tamano;
    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD();
        nuevo.setInfo(info);
        nuevo.setSiguiente(this.cabecera.getSiguiente());
        //El anterior de nuevo nodo ES la cabecera
        nuevo.setAnterior(this.cabecera);
        //El siguiente de cabecera es el nuevo nodo
        this.cabecera.setSiguiente(nuevo);
        //El siguiente del nuevo nodo SU anterior ES el nuevo nodo
        nuevo.getSiguiente().setAnterior(nuevo);
        //Aumentar la cardinalidad
        this.tamano++;
    }

    public void insertarFin(T info) {
        NodoD<T> nuevo = new NodoD();
        nuevo.setInfo(info);
        //El anterior de nuevo es el anterior de cabecera
        nuevo.setAnterior(this.cabecera.getAnterior());
        //El siguiente de nuevo es cabecera
        nuevo.setSiguiente(this.cabecera);
        //El anterior de cabecera su siguiente ES nuevo nodo
        this.cabecera.getAnterior().setSiguiente(nuevo);
        //El anterior de cabecera es ahora nuevo
        this.cabecera.setAnterior(nuevo);
        //Aumento cardinalidad
        this.tamano++;
    }

    public boolean esVacia() {
        // Método 1: tamano ==0 
        // Método 2:
        return this.cabecera == this.cabecera.getSiguiente() && this.cabecera == this.cabecera.getAnterior();
    }

    @Override
    public String toString() {
        String msg = "ListaCD{";

        for (NodoD<T> x = this.cabecera.getSiguiente(); x != this.cabecera; x = x.getSiguiente()) {
            msg += x.getInfo().toString() + "<-->";
        }

        return msg + "}";
    }

    public T eliminar(int pos) {
        /*
        Comprobar que pos (posición) sea válida  >0 && < cardinalidad xxx
        Buscar el nodo actual dada la posición  getPos(…) xxxx
        Coloco nodo anterior = actual.getAnt() xxxx
        --->
        nodoAnt su siguiente Es el siguiente de nodo actual xxx
        El siguiente de nodo actual su anterior es nodoAnt
        Cardinilidad –
        desunir nodoActual (free(..))
        Retornar el info del nodo actual
         */
        try {
            NodoD<T> nodoActual = this.getPos(pos);
            NodoD<T> nodoAnt = nodoActual.getAnterior();
            nodoAnt.setSiguiente(nodoActual.getSiguiente());
            nodoActual.getSiguiente().setAnterior(nodoAnt);
            this.tamano--;
            this.desUnir(nodoActual);
            return nodoActual.getInfo();

        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }

    }

    private void desUnir(NodoD<T> x) {
        x.setAnterior(x); //x.setAnt(null)
        x.setSiguiente(x);////x.setSig(null)
    }

    private NodoD<T> getPos(int pos) throws Exception {

        if (pos < 0 || pos >= this.tamano) {
            throw new Exception("La posición " + pos + " no es válida en la lista");
        }

        NodoD<T> nodoPos = this.cabecera.getSiguiente();
        //Se optimizo el metodo getPos reduciendo las iteraciones cuando el numero era mayor a la mitad del tamaño
        if (pos < this.tamano / 2) {
            while (pos-- > 0) {
                nodoPos = nodoPos.getSiguiente();
            }
        } else {
            int x = this.tamano - pos - 1;
            nodoPos = this.cabecera.getAnterior();
            while (x-- > 0) {
                nodoPos = nodoPos.getAnterior();
            }
        }
        return nodoPos;

    }

    /**
     * Corta los nodos que estén en posInicial hasta posFinal (incluuyendolos)
     * Ejemplo: L=<8,6,7,3,2,5> l2=L.cortar(1,3)--> l2=<6,7,3> y L=<8,2,5>
     * Condición: NO SE PERMITEN CREAR NODOS Condición: posInicial <= posFinal
     * @param posInicial una posición de la lista
     * @param posFinal una posición de la lista
     * @return una lista con los elementos desde posInicial a posFinal
     */
    public ListaCD<T> cortar(int posInicial, int posFinal)throws Exception{
        if(posFinal<posInicial){
        throw new Exception("La posicion final no puede ser nayor a la final");
        }
        ListaCD<T> l2 = new ListaCD<T>();
        //Se referencia un nodo inicial
        NodoD<T> nodoInicial = this.cabecera.getSiguiente();
        //se referencia un nodo final
        NodoD<T> nodoFinal = this.cabecera.getSiguiente();
        // contador para hallar la posicion inicial
        int x=0;
        //Se hallan las posiciones iniciales y finales
        while (posFinal-- > 0) {
            if (x == posInicial) {
                nodoInicial = nodoFinal;
            }
            x++;
            nodoFinal = nodoFinal.getSiguiente();
        }
        //Se reenlaza el numero anterior a la poscion inicial con el siguiente a la posicion final
        nodoInicial.getAnterior().setSiguiente(nodoFinal.getSiguiente());
        //Se reenlaza el numero siguiente a la poscion final con el anterior a la posicion inicial
        nodoFinal.getSiguiente().setAnterior(nodoInicial.getAnterior());
        //Se enlaza la cabeza de la lista 2 con la posicion incial
         l2.cabecera.setSiguiente(nodoInicial);
        //Se enlaza la cabeza de la lista 2 con la posicion Final
         l2.cabecera.setAnterior(nodoFinal);
         //Se enlaza la posicion incial con la cabeza de la lista 2 
         nodoInicial.setAnterior(l2.cabecera);
         //Se enlaza la posicion final con la cabeza de la lista 2 
         nodoFinal.setSiguiente(l2.cabecera);

        return l2;
    }

}
